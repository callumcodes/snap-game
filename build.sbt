name := "snap-game"

version := "0.1"

scalaVersion := "2.13.1"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.1" % "test"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.1" % "test"

Compile/mainClass := Some("client.Main")