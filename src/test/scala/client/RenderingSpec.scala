package client

import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import models._
import org.scalatest.AppendedClues

class RenderingSpec extends AnyFlatSpecLike with Matchers with AppendedClues {

  "render" should "make create card strings with numbers and symbols" in {
    def renderAssert(card: Card, expected: String) = Rendering.cardRender.render(card) shouldBe expected withClue card

    renderAssert(Card(Ace, Spades), "A♠")
    renderAssert(Card(Ten, Hearts), "10♥")
    renderAssert(Card(Two, Clubs), "2♣")
    renderAssert(Card(Queen, Diamonds), "Q♦")
  }
}
