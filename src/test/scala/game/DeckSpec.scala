package game

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class DeckSpec extends AnyFlatSpec with Matchers {

  "the deck" should "have 52 unique cards" in {
    Deck.cards.size shouldBe 52
    Deck.cards.distinct.size shouldBe 52
  }

}
