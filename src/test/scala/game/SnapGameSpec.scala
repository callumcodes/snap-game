package game

import models._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalacheck.Prop.forAll
import org.scalatest.OptionValues

import scala.annotation.tailrec

class SnapGameSpec extends AnyFlatSpec with Matchers with OptionValues {

  "SnapGame" should "start with a shuffled deck" in {
    SnapGame().cardDeck shouldNot contain theSameElementsInOrderAs Deck.cards
  }

  it should "start with an empty card pile" in {
    SnapGame().cardPile shouldBe empty
  }

  it should "have 2 default players with empty card piles" in {
    SnapGame().players should have size 2
    SnapGame().players.values.flatten shouldBe empty
  }

  "Deal" should "take the top card of the deck and move it to the card pile" in {
    val initialGame = SnapGame()
    val cardToMove = initialGame.cardDeck.head

    val dealtGame: SnapGame = initialGame.deal()
    dealtGame.cardPile should contain only cardToMove
    dealtGame.cardDeck should not contain cardToMove
  }

  it should "be able to deal the whole deck" in {
    val game = SnapGame()

    @tailrec
    def dealAllTheThings(game: SnapGame, counter: Int): SnapGame = {
      if (counter <= 0) game
      else dealAllTheThings(game.deal(), counter - 1)
    }

    val dealt = dealAllTheThings(game, 100)
    dealt.cardDeck shouldBe empty
    dealt.cardPile shouldBe game.cardDeck.reverse
  }

  "isSnap" should "be false if there is less than two cards dealt" in {
    SnapGame().isSnap shouldBe false
    SnapGame().deal().isSnap shouldBe false
  }

  "asking for more than one deck of cards" should "shuffle that many into the starting deck" in {
    Range(1, 5).foreach { i =>
      SnapGame(numberOfDecks = i).cardDeck should have size i * 52
    }
  }
  "snap" should "should transfer the card pile to the player" in {
    val initialState = new SnapGame(
      deck = List.empty,
      cardPile = List(Card(Ace, Spades), Card(Queen, Spades)),
      snapMode = Both,
      Map("1" -> List.empty, "2" -> List.empty)
    )

    val newState: SnapGame = initialState.snap("1")
    newState.cardPile shouldBe empty
    newState.players.get("1").value should contain theSameElementsInOrderAs initialState.cardPile
    newState.players.get("2").value shouldBe empty
  }

  it should "do nothing if the snap is not valid" in {
    val initialState = new SnapGame(
      deck = List.empty,
      cardPile = List(Card(Ace, Spades), Card(Queen, Hearts)),
      snapMode = Both,
      Map("1" -> List.empty, "2" -> List.empty)
    )

    val newState: SnapGame = initialState.snap("1")
    newState.cardPile should contain theSameElementsInOrderAs initialState.cardPile
    newState.players.get("1").value shouldBe empty
    newState.players.get("2").value shouldBe empty
  }

  "the winner" should "be the one with the most cards" in {
    val players = Map(
      "Other" -> List(Card(Ace, Clubs)),
      "Winner" -> List(Card(Ace, Spades), Card(Queen, Spades)),
      "Loser" -> List(Card(Eight, Hearts))
    )

    val game = new SnapGame(deck = List.empty, cardPile = List.empty, snapMode = Both, players = players)
    game.winner.value shouldBe "Winner"
  }

  it should "only be declared with the deck is empty" in {
    val players = Map(
      "Other" -> List(Card(Ace, Clubs)),
      "Winner" -> List(Card(Ace, Spades), Card(Queen, Spades)),
      "Loser" -> List(Card(Eight, Hearts))
    )

    val game = new SnapGame(deck = List(Card(Queen, Hearts)), cardPile = List.empty, snapMode = Both, players = players)
    game.winner shouldBe None
  }

  it should "be the first player in the event of a draw" in {
    val players1 = Map(
      "a" -> List(Card(Ace, Spades), Card(Queen, Spades)),
      "b" -> List(Card(Ace, Spades), Card(Queen, Spades))
    )

    val game1 = new SnapGame(deck = List.empty, cardPile = List.empty, snapMode = Both, players = players1)
    game1.winner shouldBe Some("a")

    val players2 = Map(
      "b" -> List(Card(Ace, Spades), Card(Queen, Spades)),
      "a" -> List(Card(Ace, Spades), Card(Queen, Spades))
    )

    val game2 = new SnapGame(deck = List.empty, cardPile = List.empty, snapMode = Both, players = players2)
    game2.winner shouldBe Some("b")

  }
}
