package game

import models._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class SnapValidationSpec extends AnyWordSpec with Matchers {

  // TODO Add property based testing
  "SnapValidation" when {
    "set to snap mode values" should {
      "match on values" in {
        SnapValidation.isSnap(Values, Card(Ace, Hearts), Card(Ace, Diamonds)) shouldBe true
      }
      "not match on suits" in {
        SnapValidation.isSnap(Values, Card(Queen, Hearts), Card(Ace, Hearts)) shouldBe false
      }
    }

    "set to snap mode suits" should {
      "not match on values" in {
        SnapValidation.isSnap(Suits, Card(Ace, Hearts), Card(Ace, Diamonds)) shouldBe false
      }
      "match on suits" in {
        SnapValidation.isSnap(Suits, Card(Queen, Hearts), Card(Ace, Hearts)) shouldBe true
      }
    }

    "set to snap mode both" should {
      "match on values" in {
        SnapValidation.isSnap(Both, Card(Ace, Hearts), Card(Ace, Clubs)) shouldBe true
      }
      "match on suits" in {
        SnapValidation.isSnap(Both, Card(Queen, Spades), Card(Ace, Spades)) shouldBe true
      }
    }
  }

}
