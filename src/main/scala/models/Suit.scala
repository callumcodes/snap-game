package models

sealed trait Suit

case object Diamonds extends Suit
case object Hearts extends Suit
case object Spades extends Suit
case object Clubs extends Suit

object Suit {
  val all = List(Diamonds, Hearts, Spades, Clubs)
}