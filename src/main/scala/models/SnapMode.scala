package models

sealed trait SnapMode

case object Suits extends SnapMode
case object Values extends SnapMode
case object Both extends SnapMode