package client

import models._

trait Rendering[T] {
  def render(t: T): String
}

object Rendering {

  implicit val valueRender: Rendering[Value] = {
    case Ace => "A"
    case King => "K"
    case Queen => "Q"
    case Jack => "J"
    case Ten => "10"
    case Nine => "9"
    case Eight => "8"
    case Seven => "7"
    case Six => "6"
    case Five => "5"
    case Four => "4"
    case Three => "3"
    case Two => "2"
  }

  implicit val suitRender: Rendering[Suit] = {
    case Spades => "♠"
    case Hearts => "♥"
    case Clubs => "♣"
    case Diamonds => "♦"
  }

  implicit val cardRender: Rendering[Card] = {
    case Card(value, suit) => implicitly[Rendering[Value]].render(value) + implicitly[Rendering[Suit]].render(suit)
  }

}
