package client

import models.{Both, SnapMode, Suits, Values}

import scala.annotation.tailrec

trait Parser {

  @tailrec
  final def readUntilCorrect[A] (parser: => Either[String, A]): A = {
    parser match {
      case Right(success) => success
      case Left(err) => {
        println(err)
        readUntilCorrect(parser)
      }
    }
  }

  def parseSnapMode(input: String): Either[String, SnapMode] = {
    input.trim.toLowerCase.headOption match {
      case Some('b') => Right(Both)
      case Some('v') => Right(Values)
      case Some('s') => Right(Suits)
      case _ => Left("Please enter s, v, or b.")
    }
  }

  def readSnapMode: Either[String, SnapMode] = parseSnapMode(io.StdIn.readLine("Match on (s)uit, (v)alue or (b)oth? "))

  def parseNumberOfDecks(input: String): Either[String, Int] = {
    input.toIntOption.filter(_ > 1).toRight(left = "Enter a number greater than 1.")
  }

  def readNumberOfDecks : Either[String, Int] = parseNumberOfDecks(io.StdIn.readLine("Please enter the number of decks: "))

}
