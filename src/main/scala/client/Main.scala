package client

import client.Rendering._
import game.SnapGame
import models.{Card, SnapMode}

import scala.annotation.tailrec
import scala.util.Random

object Main extends Parser {

  def main(args: Array[String]): Unit = {

    val numberOfDecks = readUntilCorrect[Int](readNumberOfDecks)
    val snapMode = readUntilCorrect[SnapMode](readSnapMode)

    val game = SnapGame(numberOfDecks, snapMode)
    val endGame = loop(game)

    endGame.players.foreach { case (player, cards) =>
      println(s"$player - ${cards.length}")
    }

    println(endGame.winner.get + " wins!")
  }

  @tailrec
  def loop(game: SnapGame): SnapGame = {
    if (game.winner.isDefined) game
    else loop(play(game))
  }

  def play(game: SnapGame): SnapGame = {
    val random = new Random()
    if (game.isSnap) {
      val playerToSnap = playerSelect(game.players.keys.toSeq)
      println(playerToSnap + " snaps!")
      game.snap(playerToSnap)
    } else {
      val dealt = game.deal()
      printCard(dealt.cardPile.head)
      dealt
    }
  }

  val random = new Random

  def playerSelect(players: Seq[String]): String = {
    players(random.nextInt(players.length))
  }

  def printCard(card: Card): Unit = println(implicitly[Rendering[Card]].render(card))
}
