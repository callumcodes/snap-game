package game

import models._

object SnapValidation {
  def isSnap(snapMode: SnapMode, first: Card, second: Card): Boolean = {
     lazy val suitsMatch = first.suit == second.suit
     lazy val valuesMatch = first.value == second.value
     snapMode match {
       case Values => valuesMatch
       case Suits => suitsMatch
       case _ => valuesMatch || suitsMatch
     }
  }
}
