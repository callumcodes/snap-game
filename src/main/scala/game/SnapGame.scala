package game

import models._
import scala.util.Random

class SnapGame private[game](deck: List[Card], val cardPile: List[Card], snapMode: SnapMode, val players: Map[String, List[Card]]) {

  // getter for testing purposes, the client shouldn't see the deck
  private[game] val cardDeck = deck

  val isSnap: Boolean = cardPile match {
    case first :: second :: rest => SnapValidation.isSnap(snapMode, first, second)
    case _ => false
  }

  def deal(): SnapGame = deck match {
    case (h :: t) => new SnapGame(
      deck = t,
      cardPile = h :: cardPile,
      snapMode,
      players
    )
    case _ => this
  }

  def snap(playerId: String): SnapGame = {
    players.get(playerId) match {
      case Some(playerPile) if isSnap => new SnapGame(
        deck,
        cardPile = List.empty,
        snapMode,
        players updated (playerId, cardPile ++ playerPile)
      )
      case _ => this
    }
  }

  lazy val winner: Option[String] = {
    if (deck.isEmpty) {
      Some(players.toList.maxBy(_._2.length)._1)
    } else {
      None
    }
  }
}

object SnapGame {

  val DefaultPlayers = List("Alice", "Bob")

  def apply(numberOfDecks: Int = 1, snapMode: SnapMode = Both, players: Seq[String] = DefaultPlayers): SnapGame = {
    new SnapGame(
      deck = Random.shuffle(List.fill(numberOfDecks)(Deck.cards).flatten),
      cardPile = List.empty,
      snapMode = snapMode,
      players.map((_ -> List.empty)).toMap
    )
  }
}