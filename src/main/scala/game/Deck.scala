package game

import models._

object Deck {
  lazy val cards: List[Card] = for {
    value <- Value.all
    suit <- Suit.all
  } yield Card(value, suit)
}
