# Snap Game Kata

A kata that implements the game Snap.

## What is it?

This is an implementation of the card game Snap. SnapGame contains all of the game logic.
There is a simple console application provided to demonstrate an implementation with the game class and to show the game logic working outside of unit tests.
A simulation of a game is played by 2 AI players, Alice and Bob. They both have equal chance of snapping.

## How to use

* Start the game with `sbt run`. Enter the number of decks to use and whether a snap should be made on suits or value.
* The game is over when there is no cards in the deck, the player with the highest score wins

## Acceptance Criteria

* The application should ask the user how many playing card decks to play with - This is asked by the console and passed to `SnapGame.apply`
* The application should ask the user whether cards should be matched: on suit, value or both - This is asked by the console and passed to `SnapGame.apply`
* The application should shuffle the decks before play commences - `SnapGame.apply` shuffles the required number of decks
* Games of snap should then be simulated between two computer players. Cards are played one at a time and when two matching cards are dealt one after another the first player to 'shout' snap wins (include some randomness) - The console client chooses which player snaps, there is a 50-50 chance.
* The winner of each round collects all the cards dealt in this round - `SnapGame.snap` deals with this logic.
* Once all cards are exhausted the application should declare the winner based on who has the most won cards - `SnapGame.winner` will be populated once the deck is depleted

## Assumptions made

* When "both" snap mode is selected, cards matched on value *OR* suit
* In the event of a draw, player one wins because life isn't fair

## Running the tests

To run the tests use
```bash
sbt test
```
## Licence
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
